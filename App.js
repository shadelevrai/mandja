import { StatusBar } from "expo-status-bar";
import React, { useEffect, useState } from "react";
import { StyleSheet, Text } from "react-native";
import { Container } from "native-base";
import AppLoading from "expo-app-loading";;
import { SafeAreaView } from "react-native-safe-area-context";
import * as Font from "expo-font";

import NavigatorComponent from "./Components/Navigator";

export default function App() {

  const [fontIsReady, setFontIsReady] = useState(false)

  useEffect(() => {
    (async () => {
      await Font.loadAsync({
        Roboto: require("native-base/Fonts/Zocial.ttf"),
        Belgates: require("./assets/fonts/Belgates_Script.ttf"),
        Cream: require("./assets/fonts/cream.ttf"),
        Roboto_medium: require("./assets/fonts/Roboto-Medium.ttf"),
      });
      setFontIsReady(true)
    })();
  }, []);

  return (
    <Container>
      <StatusBar />
      <SafeAreaView style={{flex:1}}>
      {fontIsReady ? <NavigatorComponent /> : <AppLoading/>}
      </SafeAreaView>
    </Container>
  );
}

// const styles = StyleSheet.create({});
