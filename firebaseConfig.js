import firebase from 'firebase';
import '@firebase/auth';
import '@firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyBQmPnRt289KtJAQ9-zPRxmieS0Y7I2zOE",
    authDomain: "mandja-ea8ec.firebaseapp.com",
    projectId: "mandja-ea8ec",
    storageBucket: "mandja-ea8ec.appspot.com",
    messagingSenderId: "540498567943",
    appId: "1:540498567943:web:64f318be2381a8ea951ba2",
    measurementId: "G-TEB4PR3C9J"
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export { firebase };