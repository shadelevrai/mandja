import React from "react";
import {Button} from "native-base";
import {Text} from "react-native";
import {firebase} from "../../firebaseConfig";

export default function profileScreen({navigation,setUser}) {
  console.log(firebase.auth().currentUser)
  return (
    <>
    {firebase.auth().currentUser ? <Text>hello {firebase.auth().currentUser.email} </Text> : <Text>Noooo</Text>}
    <Button style={{width: 100, backgroundColor: "red"}} onPress={()=>navigation.navigate("Home")}>
        <Text  style={{color: "white"}}>Retour</Text>
    </Button>
      <Button
        style={{width: 100, backgroundColor: "blue"}}
        onPress={() =>
          firebase
            .auth()
            .signOut()
            .then(() =>{setUser(null), navigation.navigate("PresentationLogo")})
        }
      >
        <Text style={{color: "white"}}> se déco </Text>
      </Button>
    </>
  );
}
