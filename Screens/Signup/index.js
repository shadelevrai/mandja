import React, {useEffect, useState, useRef} from "react";
import {View, StyleSheet} from "react-native";
import {Col, Icon} from "native-base";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from "react-native-responsive-screen";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Button, Segment, Text, Picker, Item} from "native-base";
import Modal from "react-native-modal";
import {firebase} from "../../firebaseConfig";
import Geocoder from "react-native-geocoding";
import {GOOGLE_API_KEY, COLOR_GREEN} from "@env";

import uploadFirebaseStorage from "../../libs/uploadFirebaseStorage";

import LogoComponent from "../../Components/Logo";
import ImageUploadComponent from "../../Components/ImageUpload";
import InputComponent from "../../Components/Input";

Geocoder.init(GOOGLE_API_KEY);

export default function SignupScreen({navigation}) {
    const [image, setImage] = useState(null);
    const [activeSegment, setActiveSegment] = useState(1);
    const [role, setRole] = useState("mandjeur");

    const [lastName, setLastName] = useState("");
    const [firstName, setFirstName] = useState("");
    const [mail, setMail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    const [foodStyle, setFoodStyle] = useState("Burger");
    const [adress1, setAdress1] = useState("");
    const [adress2, setAdress2] = useState("");
    const [zipcode, setZipcode] = useState("");
    const [city, setCity] = useState("");

    const [isModalVisible, setModalVisible] = useState(false);

    const [showMap, setShowMap] = useState(false);

    const [latitude, setLatitude] = useState(null);
    const [longitude, setLongitude] = useState(null);

    useEffect(() => {
        if (latitude && longitude) {
            firebaseCreateUSer();
        }
    }, [latitude, longitude]);

    function dataFunc() {
        let data;
        if (role === "mandjeur") {
            const dataObj = {
                role,
                mail,
                lastName,
                firstName,
            };
            data = dataObj;
        }
        if (role === "mandjhote") {
            const dataObj = {
                role,
                mail,
                lastName,
                firstName,
                adress1,
                adress2,
                zipcode,
                city,
                foodStyle,
                latitude,
                longitude,
                daysOpen: [1, 2, 3, 4, 5],
                hoursOpen: [],
                phone: "+33656964463",
                stars: 4,
                menu: [
                    {name: "Burger à l'avocat", description: "Un excellent burger avec un avocat bio", price: 6.95},
                    {name: "Cheeseburger", description: "Un burger avec deux étages de fromage", price: 5.95},
                    {name: "Burger au bacon", description: "Un burger avec du bacon et sa sauce BBQ", price: 6.95},
                    {name: "Cheesecake", description: "Un gateau au fromage façon New-York", price: 2.95},
                ],
                // menu: [
                //     {name: "Pizza royale", description: "Tomate, jambon, mozzarella", price: 5.95},
                //     {name: "Pizza marguarita", description: "Pizza basique avec du fromage et de la tomate", price: 5.95},
                //     {name: "Pizza orientale", description: "tomate, merquez, dinde, fromage", price: 6.95},
                //     {name: "Pizza tenders", description: "Pizza au poulet à base de crème fraiche", price: 4.95},
                // ],
            };
            data = dataObj;
        }
        return data;
    }

    function firebaseCreateUSer() {
        firebase
            .auth()
            .createUserWithEmailAndPassword(mail, password)
            .then((response) => {
                const uid = response.user.uid;
                const data = {
                    id: uid,
                    ...dataFunc(),
                };
                const usersRef = firebase.firestore().collection(`users`);
                usersRef
                    .doc(uid)
                    .set(data)
                    .then(() => {
                        uploadFirebaseStorage(image.uri, uid);
                    })
                    .then(() => setModalVisible(true))
                    .catch((error) => {
                        alert(error);
                    })
                    .catch((error) => {
                        alert(error);
                    });
            });
    }

    function onSignupPress() {
        if (password !== confirmPassword) {
            alert("Les mots de passe ne sont pas identiques");
            return;
        }

        if (role === "mandjhote") {
            Geocoder.from(`${adress1} ${adress2} ${zipcode} ${city}`)
                .then((json) => {
                    const location = json.results[0].geometry.location;
                    setLatitude(location.lat);
                    setLongitude(location.lng);
                })
                .catch((error) => console.warn("adresse incorect " + error));
        }
        if (role === "mandjeur") {
            firebaseCreateUSer();
        }
    }

    useEffect(() => {
        return () => {
            setImage(null);
            setActiveSegment(1);
            setRole("mandjeur");

            setLastName("");
            setFirstName("");
            setMail("");
            setPassword("");
            setConfirmPassword("");

            setFoodStyle("Burger");
            setAdress1("");
            setAdress2("");
            setZipcode("");
            setCity("");

            setModalVisible(false);

            setLatitude(null);
            setLongitude(null);
            console.log("unmount");
        };
    }, []);

    return (
        <KeyboardAwareScrollView>
            <View style={{marginLeft: wp("5%"), marginTop: hp("2%")}}>
                <View style={{flexDirection: "row"}}>
                    <View style={{flex: 1}}>
                        <Icon type="FontAwesome5" name="arrow-left" style={{opacity: 0.5, fontSize: wp("10%")}} onPress={() => navigation.navigate("PresentationLogo")} />
                    </View>
                    <View style={{flex: 10, height: hp("8%")}}>
                        <View style={{flex: 1, flexDirection: "row", justifyContent: "center", marginLeft: wp("-6%")}}>
                            <LogoComponent colorText={COLOR_GREEN} />
                        </View>
                    </View>
                </View>
                <View style={{flex: 1, flexDirection: "row", justifyContent: "center", marginTop: hp("2%")}}>
                    <ImageUploadComponent image={image} setImage={setImage} />
                </View>
                <Segment style={styles.segment}>
                    <Button
                        first
                        active={activeSegment === 1}
                        onPress={() => {
                            setActiveSegment(1), setRole("mandjeur");
                        }}
                        style={[styles.buttonSegment, {backgroundColor: activeSegment === 1 ? COLOR_GREEN : "white"}]}
                    >
                        <Text style={{color: activeSegment === 1 ? "white" : COLOR_GREEN}}>Mandjeur</Text>
                    </Button>
                    <Button
                        last
                        active={activeSegment === 2}
                        onPress={() => {
                            setActiveSegment(2), setRole("mandjhote");
                        }}
                        style={[styles.buttonSegment, {backgroundColor: activeSegment === 2 ? COLOR_GREEN : "white"}]}
                    >
                        <Text style={{color: activeSegment === 2 ? "white" : COLOR_GREEN}}>Mandj'hôte</Text>
                    </Button>
                </Segment>
                <View style={{width: wp("90%")}}>
                    <View style={{marginTop: hp("4%")}}>
                        <InputComponent text="Nom" colorText="black" nameIcon="address-card" setData={setLastName} />
                    </View>
                    <View style={{marginTop: hp("4%")}}>
                        <InputComponent text="Prénom" colorText="black" nameIcon="address-card" setData={setFirstName} />
                    </View>
                    <View style={{marginTop: hp("4%")}}>
                        <InputComponent text="E-mail" colorText="black" nameIcon="envelope" setData={setMail} capitalize="none" typeKeyboardData="email-address" />
                    </View>
                    <View style={{marginTop: hp("4%")}}>
                        <InputComponent text="Mot de passe" colorText="black" nameIcon="lock" secure={true} setData={setPassword} capitalize="none" />
                    </View>
                    <View style={{marginTop: hp("4%")}}>
                        <InputComponent text="Répétez le mot de passe" colorText="black" nameIcon="lock" secure={true} setData={setConfirmPassword} capitalize="none" />
                    </View>
                    {role === "mandjhote" && (
                        <View style={{width: wp("60%")}}>
                            <Text style={{marginLeft: wp("2%"), marginTop: hp("4%"), fontSize: hp("2.5%"), fontFamily: "Roboto_medium", fontWeight: "bold"}}>Quelle est votre spécialité ?</Text>
                            <Picker mode="dropdown" selectedValue={foodStyle} onValueChange={(item) => setFoodStyle(item)}>
                                <Item label="Burger" value="Burger" />
                                <Item label="Pizza" value="Pizza" />
                                <Item label="Sushi" value="Sushi" />
                                <Item label="Vietnamien" value="Vietnamien" />
                                <Item label="Kebab" value="Kebab" />
                                <Item label="Poke bowl" value="Pokebowl" />
                                <Item label="Cuisine équilibrée" value="healthy" />
                                <Item label="Sandwich" value="Sandwich" />
                                <Item label="Dessert" value="Dessert" />
                                <Item label="Bagels" value="Bagels" />
                            </Picker>
                        </View>
                    )}
                    {role === "mandjhote" && (
                        <>
                            <View style={{marginTop: hp("4%")}}>
                                <InputComponent text="Votre adresse 1" colorText="black" nameIcon="map-marker" setData={setAdress1} />
                            </View>
                            <View style={{marginTop: hp("4%")}}>
                                <InputComponent text="Complément d'adresse" colorText="black" nameIcon="map-marker" setData={setAdress2} />
                            </View>
                            <View style={{marginTop: hp("4%")}}>
                                <InputComponent text="Code postale" colorText="black" nameIcon="map-marker" setData={setZipcode} typeKeyboardData="numeric" />
                            </View>
                            <View style={{marginTop: hp("4%")}}>
                                <InputComponent text="Ville" colorText="black" nameIcon="city" setData={setCity} />
                            </View>
                        </>
                    )}
                    {role === "mandjeur" &&
                        (firstName.length > 0 && lastName.length > 0 && mail.length > 0 && password.length > 0 && confirmPassword.length > 0 && image ? (
                            <View style={{alignSelf: "center", marginTop: hp("5%"), marginBottom: hp("10%")}}>
                                <Button style={{width: wp("40%"), backgroundColor: "#15e3be"}} onPress={() => onSignupPress()}>
                                    <View style={{flex: 1, flexDirection: "row", justifyContent: "center"}}>
                                        <Text style={{color: "white"}}> Valider </Text>
                                    </View>
                                </Button>
                            </View>
                        ) : (
                            <View style={{alignSelf: "center", marginTop: hp("5%"), marginBottom: hp("10%")}}>
                                <Button style={{width: wp("40%"), backgroundColor: "#15e3be", opacity: 0.3}} onPress={() => alert("Il manque des informations")}>
                                    <View style={{flex: 1, flexDirection: "row", justifyContent: "center"}}>
                                        <Text style={{color: "white"}}> Valider </Text>
                                    </View>
                                </Button>
                            </View>
                        ))}
                    {role === "mandjhote" &&
                        (firstName.length > 0 && lastName.length > 0 && mail.length > 0 && password.length > 0 && confirmPassword.length > 0 && foodStyle !== null && adress1.length > 0 && zipcode.length > 0 && city.length > 0 && image ? (
                            <View style={{alignSelf: "center", marginTop: hp("5%"), marginBottom: hp("10%")}}>
                                <Button style={{width: wp("40%"), backgroundColor: "#15e3be"}} onPress={() => onSignupPress()}>
                                    <View style={{flex: 1, flexDirection: "row", justifyContent: "center"}}>
                                        <Text style={{color: "white"}}> Valider </Text>
                                    </View>
                                </Button>
                            </View>
                        ) : (
                            <View style={{alignSelf: "center", marginTop: hp("5%"), marginBottom: hp("10%")}}>
                                <Button style={{width: wp("40%"), backgroundColor: "#15e3be", opacity: 0.3}} onPress={() => alert("Il manque des informations")}>
                                    <View style={{flex: 1, flexDirection: "row", justifyContent: "center"}}>
                                        <Text style={{color: "white"}}> Valider </Text>
                                    </View>
                                </Button>
                            </View>
                        ))}
                </View>
            </View>
            <Modal isVisible={isModalVisible}>
                <View style={{backgroundColor: "white", width: wp("90%"), height: hp("28%"), borderRadius: wp("8%")}}>
                    <View style={{flexDirection: "row", justifyContent: "center", marginTop: hp("3%")}}>
                        <Text style={{color: "#15e3be", fontSize: wp("7%"), fontFamily: "Belgates"}}>Félicitation</Text>
                    </View>
                    <View style={{flexDirection: "row", justifyContent: "center", marginTop: hp("1%")}}>
                        <Text style={{color: "black"}}>Votre compte a bien été crée</Text>
                    </View>
                    <View style={{flexDirection: "row", justifyContent: "center", marginTop: hp("3%")}}>
                        <Button onPress={() => navigation.navigate("PresentationLogo")}>
                            <Text>Je me connecte</Text>
                        </Button>
                    </View>
                </View>
            </Modal>
        </KeyboardAwareScrollView>
    );
}

const styles = StyleSheet.create({
    segment: {
        backgroundColor: null,
        marginTop: hp("2%"),
    },
    buttonSegment: {
        width: wp("35%"),
        borderColor: "black",
    },
});
