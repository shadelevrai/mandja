import {Button} from "native-base";
import React, {useRef, useEffect, useState} from "react";
import {Text, View, TouchableOpacity, StyleSheet, ScrollView, Animated} from "react-native";
import MapView, {PROVIDER_GOOGLE} from "react-native-maps";
import {firebase} from "../../firebaseConfig";
import {Icon} from "native-base";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from "react-native-responsive-screen";

import localisationBurger from "../../img/icon_localisation/icon_burger.png";
import localisationPizza from "../../img/icon_localisation/icon_pizza.png";
import localisationLegume from "../../img/icon_localisation/icon_legume.png";

import ViewMandjhoteSelectedComponent from "../../Components/ViewMandjhoteSelected";

import {COLOR_GREEN_LIGHT} from "@env";

export default function HomeScreen({navigation, extraData}) {
    const [modalBottomIsVisible, setModalBottomIsVisible] = useState(false);
    const [listOfMandjhote, setListOfMandjhote] = useState(null);
    const [mandjhoteSelected, setMandjhoteSelected] = useState({});
    const [totalAmount, setTotalAmount] = useState(0);
    const mapRef = useRef();

    useEffect(() => {
        // console.log("🚀 ~ file: index.js ~ line 17 ~ HomeScreen ~ listOfMandjhote ----------------------", listOfMandjhote);
    }, [listOfMandjhote]);

    useEffect(() => {
        firebase
            .firestore()
            .collection("users")
            .where("role", "==", "mandjhote")
            .get()
            .then((querySnapshot) => {
                let array = [];
                querySnapshot.forEach((documentSnapshot) => {
                    array.push(documentSnapshot.data());
                });
                setListOfMandjhote(array);
            });
    }, []);

    function showInformationsMandjhoteSelected(infoMandjhoteSelected) {
        const {firstName, id, adress1, adress2, zipcode, city, menu} = infoMandjhoteSelected;
        setMandjhoteSelected((prevState) => ({
            ...prevState,
            firstName,
            id,
            adress1,
            adress2,
            zipcode,
            city,
            menu,
        }));
        setModalBottomIsVisible(true);
    }

    return (
        <>
            <View style={{flex: 1}}>
                <MapView ref={mapRef} provider={PROVIDER_GOOGLE} style={{flex: 1}} initialRegion={{latitude: 48.856614, longitude: 2.3522219, latitudeDelta: 0.1433501103269137, longitudeDelta: 0.14989960938692093}} showsUserLocation={true} onPress={() => setModalBottomIsVisible(false)}>
                    {listOfMandjhote?.map((item) => (
                        <View key={item.id}>
                            <MapView.Marker
                                image={(item.foodStyle === "burger" && localisationBurger) || (item.foodStyle === "pizza" && localisationPizza)}
                                coordinate={{latitude: item.latitude, longitude: item.longitude}}
                                onPress={() => {
                                    showInformationsMandjhoteSelected(item); /*, animationFunc();*/
                                }}
                            ></MapView.Marker>
                        </View>
                    ))}
                </MapView>
                <TouchableOpacity style={[StyleSheet.absoluteFill, {width: wp("8%"), height: hp("5%"), marginLeft: wp("90%"), marginTop: hp("2%")}]}>
                    <Icon type="FontAwesome5" name="cog" style={{fontSize: wp("8%")}} onPress={() => navigation.navigate("Profile")} />
                </TouchableOpacity>

                {modalBottomIsVisible && (
                    <TouchableOpacity activeOpacity={1} style={[StyleSheet.absoluteFill]}>
                        <ViewMandjhoteSelectedComponent mandjhoteSelected={mandjhoteSelected} setModalBottomIsVisible={setModalBottomIsVisible} setTotalAmount={setTotalAmount} />
                    </TouchableOpacity>
                )}
                {/* <TouchableOpacity activeOpacity={1} style={[StyleSheet.absoluteFill]}>
                    <View style={{width: wp("10%"), height: hp("4%"), backgroundColor: COLOR_GREEN_LIGHT}}>
                        <Text> {totalAmount} </Text>
                    </View>
                </TouchableOpacity> */}
            </View>
        </>
    );
}
