import React, {useState, useRef, useEffect} from "react";
import {StyleSheet, View, Text, Animated} from "react-native";
import {Button} from "native-base";
import {COLOR_GREEN} from "@env";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from "react-native-responsive-screen";

import {firebase} from "../../firebaseConfig";

import ChooseMethodLoginComponent from "../../Components/ChooseMethodLogin";
import LogoComponent from "../../Components/Logo";

export default function PresentationLogo({navigation, extraData,setUser}) {
  // console.log("🚀 ~ file: index.js ~ line 12 ~ PresentationLogo ~ extraData", extraData)
  const fadeAnimBackground = useRef(new Animated.Value(0)).current;
  const fadeAnimTitle = useRef(new Animated.Value(0)).current;
  const ball = useRef(new Animated.ValueXY({x: 0, y: hp("75%")})).current;

  // const [user, setUser] = useState(false);

  useEffect(() => {
    // if (extraData === null) {
      // console.log("🚀 ~ file: index.js ~ line 21 ~ useEffect ~ extraData", extraData)
      // navigation.navigate("Home");
    // }

    Animated.timing(fadeAnimBackground, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();

    Animated.sequence([
      Animated.delay(1000),
      Animated.timing(fadeAnimTitle, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true,
      }),
    ]).start();

    Animated.sequence([
      Animated.delay(2000),
      Animated.timing(ball, {
        toValue: {x: 0, y: hp("15%")},
        duration: 1000,
        useNativeDriver: false,
      }),
    ]).start();

    // const usersRef = firebase.firestore().collection("users");
    // firebase.auth().onAuthStateChanged((user) => {
    //   if (user) {
    //     usersRef
    //       .doc(user.uid)
    //       .get()
    //       .then((document) => {
    //         const userData = document.data();
    //         setUser(userData);
    //       })
    //       .catch((error) => {
    //         console.error("file: App.js ~ line 34 ~ firebase.auth ~ error", error);
    //       });
    //   } else {
    //     console.log("aucun user");
    //   }
    // });
  }, []);

  return (
    <>
      <Animated.View style={[styles.colorBackground, {opacity: fadeAnimBackground}]}>
        <Animated.View style={[styles.titleView, {opacity: fadeAnimTitle}]}>
          {/* <Text style={styles.titleText}>Mandja Mandja</Text> */}
          <LogoComponent />
          <Text style={styles.subtitleText}>Mandjons ensemble</Text>
        </Animated.View>
        {/* {!user && ( */}
        <Animated.View style={[styles.viewLogin, ball.getLayout()]}>
          <ChooseMethodLoginComponent navigation={navigation} setUser={setUser} />
        </Animated.View>
        {/* )} */}
      </Animated.View>
    </>
  );
}

const styles = StyleSheet.create({
  colorBackground: {
    flex: 1,
    backgroundColor: COLOR_GREEN,
  },
  titleView: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp("15%"),
    height: hp("15%"),
  },
  titleText: {
    fontFamily: "Belgates",
    fontSize: wp("10%"),
    color: "white",
  },
  subtitleText: {
    fontFamily: "Cream",
    fontSize: wp("5%"),
    color: "yellow",
    marginTop: hp("-1%"),
  },
  viewLogin: {
    backgroundColor: "white",
    height: hp("60%"),
    width: wp("100%"),
    borderRadius: wp("5%"),
  },
});
