import React, {useEffect, useState} from "react";
import {firebase} from "../../firebaseConfig";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";

import PresentationLogoScreen from "../../Screens/PresentationLogo";
import SignupScreen from "../../Screens/Signup";
import HomeScreen from "../../Screens/Home";
import ProfileScreen from "../../Screens/Profile";

// import firebasOnAuthStateChanged from "../../libs/firebase"

const Stack = createStackNavigator();

export default function NavigatorComponent() {
  const [user, setUser] = useState(null);
  
  useEffect(() => {
    firebaseGetUser();
  }, []);

  function firebaseGetUser() {
    const usersRef = firebase.firestore().collection("users");
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        usersRef
          .doc(user.uid)
          .get()
          .then((document) => {
            const userData = document.data();
            setUser(userData);
          })
          .catch((error) => {
            console.error("file: App.js ~ line 34 ~ firebase.auth ~ error", error);
          });
      } else {
        console.log("aucun user");
      }
    });
  }

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        {user ? <Stack.Screen name="Home">{(props) => <HomeScreen {...props} extraData={user} />}</Stack.Screen> :  <Stack.Screen name="PresentationLogo">{(props) => <PresentationLogoScreen {...props} extraData={user} setUser={setUser} />}</Stack.Screen>}
        <Stack.Screen name="Profile">
          {(props)=><ProfileScreen {...props} setUser={setUser} />}
        </Stack.Screen>
        <Stack.Screen name="Sign-up" component={SignupScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
