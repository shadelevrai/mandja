import React, {useEffect} from "react";
import * as ImagePicker from "expo-image-picker";
import {View, Text, Image, TouchableOpacity} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from "react-native-responsive-screen";

import * as ImageManipulator from "expo-image-manipulator";

export default function ImageUploadComponent({image, setImage}) {
  async function pickImage() {
    if (Platform.OS !== "web") {
      const {status} = await ImagePicker.requestMediaLibraryPermissionsAsync();

      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 4],
      });

      if (!result.cancelled) {
        const manipResult = await ImageManipulator.manipulateAsync(result.uri, [{resize: {width: 600, height: 600}}], {format: ImageManipulator.SaveFormat.PNG});
        setImage(manipResult);
      }

      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
  }

  return (
    <View style={{width: wp("18%"), height: hp("10%"), borderRadius: wp("18%"), borderWidth: 1, alignItems: "center", justifyContent: "center"}} onPress={pickImage}>
      {image ? (
        <TouchableOpacity onPress={() => pickImage()}>
          <Image source={{uri: image.uri}} style={{width: wp("18%"), height: hp("10%"), borderRadius: wp("18%")}} />
        </TouchableOpacity>
      ) : (
        <Text style={{fontSize: wp("2.5%"), width: wp("10%")}} onPress={() => pickImage()}>
          Ajouter une photo
        </Text>
      )}
    </View>
  );
}
