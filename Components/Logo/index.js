import React from "react"
import {Text, StyleSheet} from "react-native"

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from "react-native-responsive-screen";

export default function LogoComponent({colorText = "white", fontSizeText= wp("10%")}){
    return(
        <Text style={[styles.titleText,{color:colorText, fontSize:fontSizeText}]}>Mandja Mandja</Text>
    )
}

const styles = StyleSheet.create({
    titleText: {
        fontFamily: "Belgates",
      }
})