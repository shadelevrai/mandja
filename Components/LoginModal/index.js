import React, {useState, useEffect} from "react";
import {Icon} from "native-base";
import {View, Text} from "react-native";
import Modal from "react-native-modal";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from "react-native-responsive-screen";
import {Button, Item, Input, Label} from "native-base";
import {firebase} from "../../firebaseConfig";
import {COLOR_GREEN} from "@env"

import LogoComponent from "../Logo";
import InputComponent from "../Input";

export default function LoginModalComponent({isModalVisible, setModalVisible, navigation,setUser}) {
  const [mail, setMail] = useState("");
  const [password, setPassword] = useState("");

  function onLoginPress() {
    firebase
      .auth()
      .signInWithEmailAndPassword(mail, password)
      .then((response) => {
        const uid = response.user.uid;
        const usersRef = firebase.firestore().collection("users");
        usersRef
          .doc(uid)
          .get()
          .then((firestoreDocument) => {
            if (!firestoreDocument.exists) {
              alert("Cet utilisateur n'existe paaas");
              return;
            }
            const user = firestoreDocument.data();
            setUser(user)
            setModalVisible(false);
            navigation.navigate("Home" /*, { user }*/);
          })
          .catch((error) => {
            alert(error);
          });
      })
      .catch((error) => {
        alert(error);
      });
  }
  return (
    <>
      <Modal isVisible={isModalVisible} swipeDirection="down" onSwipeComplete={() => setModalVisible(false)}>
        <View style={{flex: 1}}>
          <View style={{backgroundColor: COLOR_GREEN, width: wp("90%"), height: hp("60%"), borderRadius: wp("8%")}}>
            <Icon type="FontAwesome5" name="times" style={{opacity: 0.5, marginLeft: wp("6%"), marginTop: wp("3%"), fontSize: wp("6%")}} onPress={() => setModalVisible(false)} />
            <View style={{flexDirection: "row", justifyContent: "center"}}>
              <LogoComponent />
            </View>
            <View style={{flexDirection: "row", marginTop: hp("2%")}}>
              <View style={{width: wp("80%"), marginLeft: wp("5%")}}>
                <InputComponent text="Adresse mail" colorText="white" nameIcon="envelope" setData={setMail} typeKeyboardData="email-address" capitalize="none" />
              </View>
            </View>
            <View style={{flexDirection: "row"}}>
              <View style={{width: wp("80%"), marginLeft: wp("5%"), marginTop: hp("4%")}}>
                <InputComponent text="Mot de passe" colorText="white" nameIcon="lock" secure={true} setData={setPassword} capitalize="none" />
              </View>
            </View>
            {mail.length > 0 && password.length > 0 ? (
              <View style={{alignSelf: "center", marginTop: hp("5%")}}>
                <Button style={{width: wp("40%"), backgroundColor: "blue"}} onPress={() => onLoginPress()}>
                  <View style={{flex: 1, flexDirection: "row", justifyContent: "center"}}>
                    <Text style={{color: "white"}}> Valider </Text>
                  </View>
                </Button>
              </View>
            ) : (
              <View style={{alignSelf: "center", marginTop: hp("5%")}}>
                <Button style={{width: wp("40%")}} disabled>
                  <View style={{flex: 1, flexDirection: "row", justifyContent: "center"}}>
                    <Text style={{color: "white"}}> Valider </Text>
                  </View>
                </Button>
              </View>
            )}
          </View>
        </View>
      </Modal>
    </>
  );
}
