import React, {useEffect, useState, useRef} from "react";
import {View, Text, ScrollView, Animated , SafeAreaView} from "react-native";
import {Thumbnail, Button} from "native-base";
import firebase from "firebase";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from "react-native-responsive-screen";
import {Icon} from "native-base";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {COLOR_GREEN} from "@env";

import CardMenuItemComponent from "../CardMenuItem"

export default function ViewMandjhoteSelectedComponent({mandjhoteSelected, setModalBottomIsVisible,setTotalAmount}) {
    const margintopAnimation = useRef(new Animated.ValueXY({x: 0, y: hp("100%")})).current;
    const fadeAnimBackground = useRef(new Animated.Value(0)).current;
    const {id, firstName, adress1, adress2, zipcode, city, menu} = mandjhoteSelected;

    const [imgLink, setImgLink] = useState(null);
    const [imgMenu, setImgMenu] = useState(null);
    const [numberFoodChoice,setNumberFoodChoice] = useState(1)

    useEffect(() => {
        (async () => {
            const urlImgProfil = await firebase.storage().ref(`${id}/first/img`).getDownloadURL();
            setImgLink(urlImgProfil);
        })();

        firebase
            .storage()
            .ref(`${id}/menu`)
            .listAll()
            .then((snap) => {
                let array = [];
                let timerForCallback = 0;
                snap.items.forEach((itemRef) => {
                    itemRef.getDownloadURL().then((imgUrl) => {
                        let obj = {nameMenu: itemRef.name, linkImgMenu: imgUrl};
                        array.push(obj);
                        timerForCallback++;
                        if (timerForCallback === snap.items.length) {
                            setImgMenu(array);
                        }
                    });
                });
            });

        Animated.timing(fadeAnimBackground, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
        }).start();

        Animated.timing(margintopAnimation, {
            toValue: {x: 0, y: hp("20%")},
            duration: 300,
            useNativeDriver: false,
        }).start();
    }, []);

    useEffect(() => {
        return () => {
            console.log("unmount");
            setImgLink(null);
            setImgMenu([]);
        };
    }, []);

    useEffect(() => {
        // console.log("🚀 ~ file: index.js ~ line 92 ~ useEffect ~ imgMenu", imgMenu);
    }, [imgMenu]);

    return (
        <Animated.View style={[margintopAnimation.getLayout(), {width: wp("100%"), height: hp("80%"), backgroundColor: COLOR_GREEN, borderRadius: wp("6%")}]}>
            <SafeAreaView>
            <ScrollView>
                <Icon type="FontAwesome5" name="times" style={{marginLeft: wp("90%"), marginTop: hp("1%"), opacity: 0.5}} onPress={() => setModalBottomIsVisible(false)} />
                <Animated.View style={{opacity: fadeAnimBackground}}>
                    <View style={{flex: 1, flexDirection: "row", marginTop: hp("1%")}}>
                        <View style={{flex: 3, marginLeft: wp("2%")}}>
                            <Thumbnail large source={{uri: imgLink}} large />
                        </View>
                        <View style={{flex: 5}}>
                            <Text style={{color: "white", fontSize: wp("5%"), fontWeight: "bold"}}> {firstName} </Text>
                            <Text style={{color: "white", fontSize: wp("4%")}}>
                                {adress1} {adress2}
                            </Text>
                            <Text style={{color: "white", fontSize: wp("4%")}}>
                                {zipcode} {city}
                            </Text>
                            <View style={{flex: 1, flexDirection: "row"}}>
                                <Icon style={{opacity: 0.7, color: "yellow", fontSize: wp("7%")}} type="Ionicons" name="star" />
                                <Icon style={{opacity: 0.7, color: "yellow", fontSize: wp("7%")}} type="Ionicons" name="star" />
                                <Icon style={{opacity: 0.7, color: "yellow", fontSize: wp("7%")}} type="Ionicons" name="star" />
                                <Icon style={{opacity: 0.7, color: "yellow", fontSize: wp("7%")}} type="Ionicons" name="star" />
                                <Icon style={{opacity: 0.7, color: "yellow", fontSize: wp("7%")}} type="Ionicons" name="star-outline" />
                            </View>
                        </View>
                        <View style={{flex: 2}}>
                            <View style={{width: wp("13%"), height: hp("6.5%"), backgroundColor: "#4e8f89", borderRadius: wp("10%")}}>
                                <Icon type="FontAwesome5" name="comment-alt" style={{fontSize: wp("8%"), marginLeft: wp("2.5%"), marginTop: hp("1.3%"), color: "white"}} />
                            </View>
                        </View>
                        <View style={{flex: 2}}>
                            <View style={{width: wp("13%"), height: hp("6.5%"), backgroundColor: "#4e8f89", borderRadius: wp("10%")}}>
                                <Icon type="FontAwesome5" name="phone-alt" style={{fontSize: wp("8%"), marginLeft: wp("2%"), marginTop: hp("1%"), color: "white"}} />
                            </View>
                        </View>
                    </View>
                    <View style={{flex: 5, marginTop: hp("3%")}}>
                        {imgMenu &&
                            menu.map(item => <CardMenuItemComponent item={item} imgMenu={imgMenu} numberFoodChoice={numberFoodChoice} setNumberFoodChoice={setNumberFoodChoice} setTotalAmount={setTotalAmount}/>)}
                    </View>
                </Animated.View>
            </ScrollView>
            </SafeAreaView>
        </Animated.View>
    );
}
