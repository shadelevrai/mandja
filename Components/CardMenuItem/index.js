import React, {useState} from "react";
import {Text, View} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from "react-native-responsive-screen";
import {Button, Thumbnail} from "native-base";
import Modal from "react-native-modal";
import {COLOR_GREEN_LIGHT} from "@env";
import {Icon} from "native-base";

export default function CardMenuItemComponent({item, imgMenu, numberFoodChoice, setNumberFoodChoice,setTotalAmount}) {
    const [isModalVisible, setModalVisible] = useState(false)

    // useEffect(() => {
    //     return () => {
    //         setNumberFoodChoice(1);
    //         console.log("");
    //     };
    // }, []);

    function lessNumberFoodChoice() {
        if (numberFoodChoice > 1) {
            setNumberFoodChoice(numberFoodChoice - 1)
        }
    }

    function plusNumberFoodChoice() {
        setNumberFoodChoice(numberFoodChoice + 1)
    }



    const urlLinkImgMenu = imgMenu.find((a) => a.nameMenu === `${item.name}.png`);
    return (
        <View key={item.firstName}>
            <View style={{marginLeft: wp("4%"), width: wp("90%"), height: hp("19%"), backgroundColor: COLOR_GREEN_LIGHT, padding: wp("3%"), marginBottom: hp("2%")}}>
                <View style={{flex: 1, flexDirection: "row"}}>
                    <View style={{flex: 5}}>
                        <Text style={{color: "white", fontSize: wp("4.5%")}}> {item.name} </Text>
                        <Text style={{opacity: 0.5, fontSize: wp("3.5%"), height: hp("4%")}}> {item.description} </Text>
                        <Text style={{marginTop: hp("1%"), fontSize: wp("3%")}}> {item.price}€ </Text>
                        <Button style={{backgroundColor: "white", height: hp("4%"), width: "50%", marginLeft: wp("25%"), marginTop: hp("1%")}} onPress={() => setModalVisible(true)}>
                            <Text style={{fontSize: wp("3%"), marginLeft: wp("4.5%")}}>Ajout au panier</Text>
                        </Button>
                    </View>
                    <View style={{flex: 2}}>{urlLinkImgMenu && <Thumbnail square large source={{uri: urlLinkImgMenu.linkImgMenu}} style={{marginTop: hp("3%"), marginLeft: "10%"}} />}</View>
                </View>
            </View>
            {isModalVisible && (
                <Modal
                    isVisible={isModalVisible}
                    swipeDirection="down"
                    onSwipeComplete={() => {
                        setModalVisible(false), setNumberFoodChoice(1)
                    }}
                >
                    <View style={{width: wp("70%"), height: hp("30%"), backgroundColor: COLOR_GREEN_LIGHT, marginLeft: wp("9%"), marginTop: hp("20%")}}>
                        <View style={{alignItems: "center", marginTop: hp("1%")}}>
                            <Text style={{color: "white", fontSize: wp("5%")}}> {item.name} </Text>
                        </View>
                        <View style={{alignItems: "center", marginTop: hp("1%")}}>{urlLinkImgMenu && <Thumbnail square large source={{uri: urlLinkImgMenu.linkImgMenu}} />}</View>
                        <View style={{width: wp("30%"), height: hp("4%"), marginLeft: wp("20%"), marginTop: hp("1%")}}>
                            <View style={{flex: 1, flexDirection: "row", justifyContent: "space-between"}}>
                                <Icon type="Ionicons" name="remove-circle-outline" style={{color: "white"}} onPress={() => lessNumberFoodChoice()} />
                                <Text style={{color: "white", fontSize: wp("6%")}}>{numberFoodChoice}</Text>
                                <Icon type="Ionicons" name="add-circle-outline" style={{color: "white"}} onPress={() => plusNumberFoodChoice()} />
                            </View>
                        </View>
                        <View style={{flex: 1, flexDirection: "row", justifyContent: "center"}}>
                            <Button style={{backgroundColor: "white", height: hp("4%"), width: wp("30%"), marginTop: hp("1%"), borderRadius: wp("0.5%")}} onPress={()=>setModalVisible(false)}>
                                <Text style={{fontSize: wp("3%"), marginLeft: wp("2%")}}>Total {item.price * numberFoodChoice} </Text>
                            </Button>
                        </View>
                    </View>
                </Modal>
            )}
        </View>
    );
}
