import {View, Icon, Text} from "native-base";
import React, {useRef, useEffect, useState} from "react";
import {Animated, StyleSheet, Button} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from "react-native-responsive-screen";
import * as Font from "expo-font";
import Modal from "react-native-modal";

import LoginModalComponent from "../LoginModal";

export default function ChooseMethodLoginComponent({navigation,setUser}) {
  const fadeAnimBackground = useRef(new Animated.Value(0)).current;

  const [isModalVisible, setModalVisible] = useState(false);

  function toggleModal() {
    setModalVisible(!isModalVisible);
  }

  useEffect(() => {
    Animated.sequence([
      Animated.delay(3000),
      Animated.timing(fadeAnimBackground, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true,
      }),
    ]).start();
  }, []);

  return (
    <>
      <Animated.View style={[styles.generalView, {opacity: fadeAnimBackground}]}>
        <View style={styles.alignItemsCenter}>
          <Text style={styles.titleText}>Se connecter</Text>
        </View>
        <View style={styles.buttonCustomView}>
          <View style={[styles.buttonCustomViewStyle, {backgroundColor: "#07e5ff"}]}>
            <View style={{flexDirection: "row", flex: 1, alignSelf: "center", alignItems: "center"}}>
              <Icon type="FontAwesome5" name="envelope" style={{color: "white"}} />
              <Text style={{color: "white", fontWeight: "bold", fontSize: hp("2.5%")}} onPress={() => toggleModal()}>
                {"   "}
                Via l'e-mail
              </Text>
            </View>
          </View>
          <View style={[styles.buttonCustomViewStyle, {backgroundColor: "#295dc4"}]}>
            <View style={{flexDirection: "row", flex: 1, alignSelf: "center", alignItems: "center"}}>
              <Icon type="FontAwesome5" name="facebook-square" style={{color: "white"}} />
              <Text style={{color: "white", fontWeight: "bold", fontSize: hp("2.5%")}}>
                {"   "}
                Via Facebook
              </Text>
            </View>
          </View>
        </View>
        <View>
          <Text style={{fontWeight: "bold", marginTop: hp("4%"), fontSize: wp("4%")}}>Vous n'êtes pas encore inscrit ? </Text>
          <Text style={{color: "#295dc4", marginTop: hp("1%"), fontSize: wp("4%")}} onPress={() => navigation.navigate("Sign-up")}>
            Inscription
          </Text>
        </View>
      </Animated.View>
      <LoginModalComponent isModalVisible={isModalVisible} setModalVisible={setModalVisible} navigation={navigation} setUser={setUser}/>
    </>
  );
}

const styles = StyleSheet.create({
  generalView: {
    marginLeft: wp("7%"),
    width: wp("85%"),
    marginTop: hp("2%"),
  },
  alignItemsCenter: {
    alignItems: "center",
  },
  titleText: {
    fontFamily: "Roboto_medium",
    opacity: 0.5,
    fontSize: hp("2.5%"),
  },
  buttonCustomView: {
    // flex: 1,
    // flexDirection: "column",
    alignSelf: "center",
    // justifyContent: 'center',
    marginTop: hp("2%"),
  },
  buttonCustomViewStyle: {
    height: hp("6%"),
    width: wp("70%"),
    borderRadius: wp("7%"),
    marginTop: hp("2%"),
  },
});
