import React from "react";
import {View} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from "react-native-responsive-screen";
import {Icon, Label, Item, Input} from "native-base";

export default function InputComponent({text, colorText, nameIcon, secure = false, setData, capitalize = "sentences",typeKeyboardData="default"}) {
  return (
    <View>
      <Label style={{marginLeft: wp("2%"), marginTop: hp("-1%"), fontSize: hp("2.5%"), fontFamily: "Roboto_medium", fontWeight: "bold", color: colorText}}>{text}</Label>
      <Item regular style={{marginTop: hp("1%"), backgroundColor: "white"}}>
        <Icon type="FontAwesome5" name={nameIcon} style={{opacity: 0.5}} />
        <Input secureTextEntry={secure} onChangeText={(text) => setData(text)} autoCapitalize={capitalize} keyboardType={typeKeyboardData} />
      </Item>
    </View>
  );
}
