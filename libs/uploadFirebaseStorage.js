import {firebase} from "../firebaseConfig"

export default async function uploadFirebaseStorage(uri,userID){
    const response = await fetch(uri);
      const blob = await response.blob();  
      const ref = firebase.storage().ref(userID).child(`first/img`);
      return ref.put(blob);
}